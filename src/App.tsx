import React, { ChangeEvent, Component, Fragment, FormEvent } from 'react';
import axios from 'axios';
import './App.scss';
import { NumberRequiredInput } from './components/numberRequiredInput';
import { TimeRequiredInput } from './components/timeRequiredInput';
import { RequiredInput, RequiredInputProps } from './components/requiredInput';
import { Loader } from './components/loader';

enum DishType {
  PIZZA = 'pizza',
  SOUP = 'soup',
  SANDWICH = 'sandwich',
}

interface AppState {
  dishType?: DishType;
  isValid: boolean;
  sending: boolean;
  success: boolean;
  error?: string;
}

export default class App extends Component {

  state: AppState = {
    dishType: DishType.PIZZA,
    isValid: false,
    sending: false,
    success: false
  }

  private requiredInputs: Array<RequiredInput<RequiredInputProps> | null> = [];

  render(): any {
    this.requiredInputs = [];

    return (
      <div className="app">
        <Loader showWhen={this.state.sending}></Loader>
        <div className="form-header">Send me your dish</div>
        <form onSubmit={this.sumbitForm}>
          <RequiredInput
            key="name"
            ref={(el: RequiredInput<RequiredInputProps>) => { this.requiredInputs.push(el) }}
            label="Dish name"
            name="name"
            onChange={this.validateForm}>
          </RequiredInput>
          <TimeRequiredInput
            key="preparation_time"
            ref={(el: TimeRequiredInput) => { this.requiredInputs.push(el) }}
            label="Preparation Time"
            name="preparation_time"
            onChange={this.validateForm}>
          </TimeRequiredInput>
          <label>
            <span className="label">Type</span>
            <select
              name="dishType"
              onChange={this.changeDishType}
              value={this.state.dishType}>
              {[DishType.PIZZA, DishType.SOUP, DishType.SANDWICH].map((dishType: DishType) => {
                return (<option value={dishType} key={dishType}>{dishType}</option>);
              })}
            </select>
          </label>
          {this.renderDishSpecificField()}
          <div className="submit">
            <button disabled={!this.state.isValid}>Submit</button>
          </div>
        </form>
        {this.renderSuccessMessage()}
      </div>
    );
  }

  private changeDishType = (e: ChangeEvent<HTMLSelectElement>) => {
    this.setState({
      dishType: e.target.value
    }, () => {
      this.validateForm();
    });

  }

  private sumbitForm = (e: FormEvent): void => {
    e.preventDefault();
    this.setState({
      sending: true
    });
    let data: any = {
      type: this.state.dishType
    };
    this.requiredInputs.forEach((requiredInput: RequiredInput<RequiredInputProps> | null) => {
      data = Object.assign(data, requiredInput ? requiredInput.getValueAsObject() : {});
    });
    axios.post(`https://frosty-wood-6558.getsandbox.com:443/dishes`, data).then((response) => {
      this.resetForm();
      this.setState({ sending: false, success: true }, () => {
        setTimeout(() => {
          this.setState({ success: false });
        }, 3000);
      });
    }).catch((error: any) => {
      this.requiredInputs.forEach((requiredInput: RequiredInput<RequiredInputProps> | null) => {
        if (requiredInput) {
          requiredInput.setValidationError(error.response.data);
        }
      });
      this.setState({ sending: false});
    });
  }

  private resetForm(): void {
    this.requiredInputs.forEach((requiredInput: RequiredInput<RequiredInputProps> | null) => {
      if (requiredInput) {
        requiredInput.reset();
      }
    });
    this.setState({
      dishType: DishType.PIZZA
    }, () => {
      this.validateForm();
    })
  }

  private validateForm = (): void => {
    this.setState({
      isValid: this.isFormValid()
    })
  }

  private isFormValid(): boolean {
    return this.requiredInputs.every((input: RequiredInput<RequiredInputProps> | null) => {
      return input ? input.isValid() : true;
    });
  }

  private renderSuccessMessage(): any {
    if (this.state.success) {
      return (
        <div className="success">Dish data successfully sent.</div>
      );
    }
    return null;
  }

  private renderDishSpecificField(): any {
    switch (this.state.dishType) {
      case DishType.PIZZA:
        return (
          <Fragment>
            <NumberRequiredInput
              key="no_of_slices"
              ref={(el: NumberRequiredInput) => { this.requiredInputs.push(el) }}
              label="# of Slices"
              name="no_of_slices"
              min={1}
              onChange={this.validateForm}>
            </NumberRequiredInput>
            <NumberRequiredInput
              key="diameter"
              ref={(el: NumberRequiredInput) => { this.requiredInputs.push(el) }}
              label="Diameter"
              name="diameter"
              min={1}
              isFloat={true}
              onChange={this.validateForm}>
            </NumberRequiredInput>
          </Fragment>
        );
      case DishType.SOUP:
        return (
          <NumberRequiredInput
            key="spiciness_scale"
            ref={(el: NumberRequiredInput) => { this.requiredInputs.push(el) }}
            label="Spiciness scale"
            min={1}
            max={10}
            name="spiciness_scale"
            onChange={this.validateForm}>
          </NumberRequiredInput>
        );

      case DishType.SANDWICH:
        return (
          <NumberRequiredInput
            key="slices_of_bread"
            ref={(el: NumberRequiredInput) => { this.requiredInputs.push(el) }}
            label="Slices of bread"
            name="slices_of_bread"
            onChange={this.validateForm}>
          </NumberRequiredInput>
        );
    }
  }
}
