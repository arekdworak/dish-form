import React from 'react';
import './loader.scss';

export interface LoaderProps {
    showWhen: boolean;
}

export const Loader = (props: LoaderProps) => {
    if (props.showWhen) {
        return (
            <div className="loadingWrapper">
                <div className="loading"></div>
            </div>
        );
    }
    return null;
}