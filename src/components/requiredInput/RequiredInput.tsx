import React, { Component, ChangeEvent } from 'react';

export interface RequiredInputProps {
    label: string;
    name: string;
    onChange: (e: ChangeEvent<HTMLInputElement>) => void;
}

export interface RequiredInputState {
    value: string;
    validationError: string;
    touched: boolean;
}

export class RequiredInput<P extends RequiredInputProps> extends Component<P> {

    state: RequiredInputState = {
        value: '',
        validationError: '',
        touched: false
    }

    render(): any {
        return (
            <label>
                <span className="label">
                    <span>{this.props.label}</span>
                    <span className="required">*</span>
                </span>
                <input
                    type={this.getType()}
                    value={this.state.value}
                    name={this.props.name}
                    onChange={this.onChange}
                    onBlur={() => { this.setState({ touched: true }) }}
                    autoComplete="off"
                    min={this.getMin()}
                    max={this.getMax()}
                    step={this.getStep()}>
                </input>
                {this.renderRequiredMessage()}
            </label>
        )
    }

    isValid(): boolean {
        return !!this.state.value;
    }

    getValueAsObject(): any {
        return {
            [this.props.name]: this.generateValue()
        }
    }

    reset(): void {
        this.setState({
            touched: false,
            value: '',
            validationError: ''
        });
    }

    setValidationError(validationError: any): void  {
        let validationErrorForThisField: string = validationError[this.props.name];
        if (validationErrorForThisField) {
            this.setState({ validationError: validationErrorForThisField});
        }
    }

    protected getType(): string {
        return 'text';
    }

    protected getMin(): number | undefined {
        return undefined;
    }

    protected getMax(): number | undefined {
        return undefined;
    }

    protected getStep(): number | undefined {
        return undefined;
    }

    protected generateValue(): any {
        return this.state.value;
    }

    private onChange = (e: ChangeEvent<HTMLInputElement>) => {
        this.setState({ value: e.target.value, validationError: '' }, () => {
            this.props.onChange(e);
        });
    }

    private renderRequiredMessage(): any {
        if (this.state.touched) {
            if (!this.isValid()) {
                return <span className="required-message">Field is required</span>
            } else if (this.state.validationError) {
                return <span className="required-message">{this.state.validationError}</span>
            }
        }
        return null;
    }
}