import { RequiredInput, RequiredInputProps, RequiredInputState } from '../requiredInput';

const DEF_TIME_VALUE: string = '00:00:00';

export class TimeRequiredInput extends RequiredInput<RequiredInputProps> {

    state: RequiredInputState = {
        ...this.state,
        value: DEF_TIME_VALUE
    }

    isValid(): boolean {
        return super.isValid() && this.state.value !== DEF_TIME_VALUE;
    }

    reset(): void {
        this.setState({ touched: false, value: DEF_TIME_VALUE });
    }

    protected getType(): string {
        return 'time'
    }

    protected getStep(): number | undefined {
        return 1;
    }

    protected generateValue(): any {
        let time: any = this.state.value;
        if (time.length === 5) { //hh:mm
            return `${time}:00`;
        }
        return time;
    }

}