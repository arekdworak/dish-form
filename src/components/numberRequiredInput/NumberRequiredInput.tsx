import { RequiredInput, RequiredInputProps } from './../requiredInput';

export interface NumberRequiredInputProps extends RequiredInputProps {
    min?: number;
    max?: number;
    isFloat?: boolean;
}

export class NumberRequiredInput extends RequiredInput<NumberRequiredInputProps> {

    isValid(): boolean {
        let result = super.isValid();
        if (this.props.isFloat) {
            result = result && parseFloat(this.state.value) !== 0
        } else {
            result = result && parseInt(this.state.value) !== 0;
        }
        return result;
    }

    protected getType(): string {
        return 'number';
    }

    protected getMin(): number | undefined {
        return this.props.min;
    }

    protected getMax(): number | undefined {
        return this.props.max;
    }

    protected getStep(): number | undefined {
        return this.props.isFloat ? 0.1 : 1;
    }

    protected generateValue(): any {
        if (this.props.isFloat) {
            return parseFloat(this.state.value);
        } else {
            return parseInt(this.state.value);
        }
    }
}